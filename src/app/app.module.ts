import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { FooterComponent } from './core/footer/footer.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { ProjetosComponent } from './pages/projetos/projetos.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { ButtonComponent } from './core/button/button.component';
import { ClientesComponent } from './pages/projetos/clientes/clientes.component';
import {RouterModule, Routes} from "@angular/router";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const appRoutes: Routes = [
    {
        path: '',
        component: InicioComponent,
    },
    {
        path: 'sobre-nos',
        component: SobreComponent,
    },
    {
        path: 'projetos',
        component: ProjetosComponent,
    },
    {
        path: 'servicos',
        component: ServicosComponent,
    },
    {
        path: 'contato',
        component: ContatoComponent,
    },
];

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        FooterComponent,
        InicioComponent,
        SobreComponent,
        ProjetosComponent,
        ServicosComponent,
        ContatoComponent,
        ButtonComponent,
        ClientesComponent,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        NgbModule,
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
