import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InicioComponent} from './pages/inicio/inicio.component';
import {SobreComponent} from './pages/sobre/sobre.component';
import {ProjetosComponent} from './pages/projetos/projetos.component';
import {ServicosComponent} from './pages/servicos/servicos.component';
import {ContatoComponent} from './pages/contato/contato.component';

const routes: Routes = [
    {
        path:'',
        component:InicioComponent
    },
    {
        path:'sobre-nos',
        component: SobreComponent
    },
    {
        path:'projetos',
        component: ProjetosComponent
    },
    {
        path:'servicos',
        component: ServicosComponent
    },
    {
        path:'contato',
        component: ContatoComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
