import {Component, HostListener, OnInit} from '@angular/core';
declare const window: any;
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor() {
    }

    public boolChangeClass: string;
    private scrolled = false;

    public navbarExpanded = false;
    public navbarShow = '';

    ngOnInit() {
        if (this.navbarExpanded != false) {
            this.navbarShow = 'show';
        } else {
            this.navbarShow = '';
        }
    }

    setScrolled(val) {
        this.scrolled = val;
        if (val == true) {
            this.boolChangeClass = 'boolChangeClass';
            console.log(this.boolChangeClass);
            if (this.scrolled == true) {
                console.log(this.scrolled);
            } else {
                this.boolChangeClass = null;
                console.log(this.scrolled);
            }
        }
    }

    @HostListener("window:scroll", [])
    onWindowScroll() {

        const number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        if (number > 20) {
            this.boolChangeClass = 'boolChangeClass'
        } else {
            this.boolChangeClass = 'undefined'
        }

    }
}